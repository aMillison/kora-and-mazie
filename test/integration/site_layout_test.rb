require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  
  test "site links" do
    
    get root_path
    assert_template "static_pages/home"
    assert_select "a[href=?]", root_path, count:2
    assert_select "a[href=?]", kora_path
    assert_select "a[href=?]", mazie_path
    assert_select "a[href=?]", together_path
  end
end
