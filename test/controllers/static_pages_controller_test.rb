require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get root_url
    assert_response :success
  end
  
  
  test "should get kora" do
    get kora_path
    assert_response :success
  end

  test "should get mazie" do
   get mazie_path
   assert_response :success
  end

  test "should get together" do
    get together_path
    assert_response :success
  end

end
