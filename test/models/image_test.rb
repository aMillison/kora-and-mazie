require 'test_helper'

class ImageTest < ActiveSupport::TestCase
  
  def setup
    @image = Image.new(path: "kora-and-mazie/app/public", animal: "mazie")
  end
  
  test "should be valid" do
    assert @image.valid?
  end
  
  test "should have animal" do
    @image.animal = "   "
    assert_not @image.valid?
  end 
  
  test "should have path" do
    @image.path = "    "
    assert_not @image.valid?
  end
  
  test "should have correct animal" do
    assert_match(@image.animal, 'kora, mazie, together') 
  end
  
  
  
end
