class Image < ApplicationRecord
  validates :animal, :path,  presence: true
  validates :animal, :path, uniqueness: true
end
