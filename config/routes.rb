Rails.application.routes.draw do
  get 'users/new'

  root 'static_pages#home'
  
  get '/home', to:'static_pages#home'
  
  get '/kora', to:'static_pages#kora'

  get '/mazie', to:'static_pages#mazie'

  get '/together', to:'static_pages#together'

 
end
