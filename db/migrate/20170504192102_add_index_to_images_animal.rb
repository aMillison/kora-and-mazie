class AddIndexToImagesAnimal < ActiveRecord::Migration[5.0]
  def change
    add_index :images, :animal, unique: true
  end
end
